#include <stdio.h>

int main()
{
	printf("hello, world\a\n");

	/* Backspace character is quite often a nondestructive action and only
	   moves the cursor back. So it won't regularly delete characters */

	printf("hello, worl \bd\n");

	/* Intended to make a printer eject out the current page wherever it
	   is and then continue printing on the next. On my terminal, it 
	   places the character where it would have been one line below almost 
	   like its original intended use. This is the same thing as a vertical
	   tab \v in some cases */
 
	printf("hello, worl\fd\n");

	/* Moves the cursor to the beginning of the line. It deletes the line in
	   terminal. Adding the newline moves the cursor to the intial point
	   of the next line. So it looks just like if I printed a normal \n */
	printf("hello, world\r\n");

	printf("hello, worl\td\n");

	printf("hello, worl\vd\n");

	printf("\\\n");

	printf("\?\n");

	printf("\'\n");

	printf("\"\n");

	/* This is cool since it converts an octal number to an equivelant byte. 
	   And then outputs the character mapped to that byte. 
	   Just use \0 and then follow it with your numbers. Same
	   for \x and then some hex numbers. Lets give it a whirl. It has a max
	   of 2 octal or hex characters*/

	printf("\077 and \xff \n");

}
